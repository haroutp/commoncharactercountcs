﻿using System;
using System.Collections.Generic;

namespace CommonCharacterCount
{
    class Program
    {
        static void Main(string[] args)
        {
            string s1 = "abca";
            string s2 = "xyzbac";
            System.Console.WriteLine(commonCharacterCount(s1, s2));
        }
        static int commonCharacterCount(string s1, string s2) {
            int counter = 0;
            List<char> l1 = new List<char>();
            List<char> l2 = new List<char>();

            foreach (var item in s1)
            {
                l1.Add(item);
            }

            foreach (var item in s2)
            {
                l2.Add(item);
            }
            

            for (int i = 0; i < s2.Length; i++)
            {
                if(l1.Contains(s2[i])){
                    counter++;
                    l1.Remove(s2[i]);
                    l2.Remove(s2[i]);
                }
            }
            

            return counter;
        }
    }
}
